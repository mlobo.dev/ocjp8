package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        Scanner in = new Scanner(System.in);

        System.out.print("Informe a quantidade de produtos: ");

        int quantity = in.nextInt();

        Product[] products = new Product[quantity];

        double sum =0;
        for (int i=0; i<products.length;i++){
            System.out.print("Informe o nome do produto: ");
            String name = in.next();

            System.out.print("Quanto custa o/a "+name+"?");
            double price = in.nextDouble();

            products[i] = new Product(name,price);
            sum += products[i].getPreco();
        }
        System.out.printf("a média dos preços é de %.2f%n",sum/products.length);
        for (int i = 0; i<products.length ; i++) {
            System.out.println("[ Nome: "+products[i].getNome()+", Preço: "+products[i].getPreco()+" ]");
        }
        System.out.println("[ Nome: Total, Preço:"+sum+" ]");
    }
}
