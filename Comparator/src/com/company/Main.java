package com.company;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Produto> produtos = new ArrayList<>();
        produtos.add(new Produto("TV", 999d));
        produtos.add(new Produto("Radio", 34d));
        produtos.add(new Produto("Micro", 234234d));
        produtos.add(new Produto("Allstar", 234234d));

        produtos.sort((p1,p2) -> p1.getNome().toUpperCase().compareTo(p2.getNome().toUpperCase()));
        for (Produto produto : produtos) {
            System.out.println(produto.getNome());
        }
    }
}
