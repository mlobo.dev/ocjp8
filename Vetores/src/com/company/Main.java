package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        Scanner in = new Scanner(System.in);
        System.out.print("Informe a quantidade de alturas que serão mensuradas:");
        int length = in.nextInt();
        double heights[] = new double[length];
        double somaAlturas = 0;

        for (int i = 0; i < heights.length; i++) {
            System.out.print("Informa a " + (i + 1) + "º ||altura: ");
            heights[i] = in.nextDouble();
            somaAlturas += heights[i];
        }

        System.out.printf("A média das alturas é de: %.6f%n",somaAlturas/heights.length );

    }
}
