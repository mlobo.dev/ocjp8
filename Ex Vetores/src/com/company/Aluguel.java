package com.company;

public class Aluguel {

    private Quarto quarto;
    private Estudante estudante;
    private Double valor;

    public Aluguel(Quarto quarto, Estudante estudante, Double valor) {
        this.quarto = quarto;
        this.estudante = estudante;
        this.valor = valor;
    }

    public Aluguel(Quarto quarto, Double valor) {
        this.quarto = quarto;
        this.valor = valor;
    }

    public Aluguel(){

    }

    public Quarto getQuarto() {
        return quarto;
    }

    public void setQuarto(Quarto quarto) {
        this.quarto = quarto;
    }

    public Estudante getEstudante() {
        return estudante;
    }

    public void setEstudante(Estudante estudante) {
        this.estudante = estudante;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }


    @Override
    public String toString() {
        return "[" +
                "quarto=" + quarto +
                ", estudante=" + estudante +
                ", valor=" + valor +
                ']';
    }
}
