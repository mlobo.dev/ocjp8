package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {


    /*
    A dona de um pensionato possui dez quartos para alugar para estudantes,
    sendo esses quartos identificados pelos números 0 a 9.
    Fazer um programa que inicie com todos os dez quartos vazios, e depois
    leia uma quantidade N representando o número de estudantes que vão
    alugar quartos (N pode ser de 1 a 10). Em seguida, registre o aluguel dos
    N estudantes. Para cada registro de aluguel, informar o nome e email do
    estudante, bem como qual dos quartos ele escolheu (de 0 a 9). Suponha
    que seja escolhido um quarto vago. Ao final, seu programa deve imprimir
    um relatório de todas ocupações do pensionato, por ordem de quarto,
    conforme exemplo.

     */

    public static void main(String[] args) {
        int quartosDisponiveis = 0;
        double totalAluguel =0;
        Scanner in = new Scanner(System.in);
        Quarto[] quartos = new Quarto[10];
        List<Aluguel> alugueis = new ArrayList<>();
        for (int i = 0; i < quartos.length; i++) {
            quartos[i] = new Quarto(i);
            Aluguel aluguel = new Aluguel(quartos[i], new Random().nextInt(500) + 100d);
            alugueis.add(aluguel);
        }

        int continuar = 0;
        while (continuar == 0) {
             quartosDisponiveis = 0;

            System.out.println("olá seja bem vindo, qual quarto o sr(a) deseja? temos os seguintes disponíveis: ");

            for (Aluguel aluguel : alugueis) {
                if (aluguel.getEstudante() == null) {
                    System.out.print("[" + aluguel.getQuarto().getNumero() + "] ");
                    quartosDisponiveis++;
                }
            }
            if (quartosDisponiveis == 0) {
                System.out.println("ops... desculpe, todos os quartos já foram reservados, tente novamente mais tarde!, obrigado!");
            } else {
                System.out.println("\ninforme o número do quarto desejado.");
                int numeroQuarto = in.nextInt();

                if ((numeroQuarto < 0 || numeroQuarto > 9) || alugueis.get(numeroQuarto).getEstudante() != null) {
                    System.err.println("Desculpe, esse quarto já foi reservado, ou ainda não está disponível, verifique os quartos disponíveis.");
                } else {
                    System.out.printf(" o Aluguel desse quarto custa R$ %.2f%n deseja confirmar a reserva?  0 = SIM, 1 = NÃO :", alugueis.get(numeroQuarto).getValor());
                    int confirmacao = in.nextInt();
                    if (confirmacao == 0) {
                        System.out.println("informe seu nome e em seguida o email.");
                        String nome = in.next();
                        String email = in.next();
                        alugueis.get(numeroQuarto).setEstudante(new Estudante(nome, email));
                        System.out.println("Reserva concluida...");
                    }
                }
            }

            System.out.println("Algo mais em que possas te ajudar? 0 = SIM, 1 = NÃO");
            continuar = in.nextInt();

        }
        System.out.println("RELATÓRIO DE LOCAÇÃO");

        for (Aluguel aluguel : alugueis) {
            if (aluguel.getEstudante() == null) {
                System.out.println("Quarto:" + aluguel.getQuarto().toString() + " Disponível");
            } else {
                System.out.println(aluguel.toString());
                totalAluguel += aluguel.getValor();
            }
        }
        System.out.println();
        System.out.println("Total de quartos disponíveis: "+quartosDisponiveis);
        System.out.println("Total do valor dos alugueis: " + totalAluguel);

    }

}
