package com.company;

public class Quarto {

    private int numero;

    public Quarto(int numero) {
        this.numero = numero;
    }

    public Quarto(){

    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "numero=" + numero;
    }
}
