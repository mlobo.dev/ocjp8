package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        List<String> nomes = new ArrayList<>();
        nomes.add("Maria");
        nomes.add("Alex");
        nomes.add("Bob");
        nomes.add("José");
        nomes.add("Jaccakn");
        nomes.add("Abraham");
        nomes.add(2, "Marco");

        listar(nomes);
        System.out.println("-------------------");
        System.out.println("removendo nome da Maria");
        System.out.println("-------------------");
        nomes.remove("Maria");
        listar(nomes);
        System.out.println("-------------------");
        System.out.println("removendo nomes que iniciam com A");
        System.out.println("-------------------");
        nomes.removeIf(x-> x.startsWith("A"));
        listar(nomes);
        System.out.println("-------------------");
        System.out.println("index of José: "+nomes.indexOf("José"));
        System.out.println("-------------------");

        List<String> result = nomes.stream().filter(x-> x.startsWith("J")).collect(Collectors.toList());
        System.out.println("Apenas letra J");
        listar(result);
        System.out.println("-------------------");

        System.out.println("Primeiro nome com a letra J");

        String primeiro = nomes.stream().filter(x-> x.startsWith("J")).findFirst().orElse("Não localizado");
        System.out.println(primeiro);


    }

    private static void listar(List<String> items) {
        for (String item : items) {
            System.out.println(item);
        }
    }
}
