package com.company;

public class Main {

    public static void main(String[] args) {
//        Variáveis cujo tipo são classes não devem ser entendidas como caixas, mas sim
//        “tentáculos” (ponteiros) para caixas

        // Os objetos ficam armazenados em 2 tipos de locais, a memória STACK e a Memória HEAP

        // No STACK ficam armazenados os objetos criados no programa
        Produto p1, p2;

        // Quando o objeto é instanciado, em tempo de execução, o objeto é alocado na memória HEAP.
        // Isso trata-se do alocamento dinâmico de memória.
        // A memória STACK guarda a referência para o local da memória HEAP.
        p1 = new Produto("TV",9000d);
        p2 = new Produto("Rádio",8000d);


        // TIPOS PRIMITIVOS SÃO TIPOS VALORS
        // os tipos primitivos armazenam seu valor na memória stack
        int a =3;
        int b =4;

        // ao fazer esse tipo de atribuição ocorre que o valor de b na memória, é copiado e atribuido para a.
        a = b;

        //VALORES PADRÃO DE OBJETOS - NEW

        Produto produto = new Produto();
        //produto.nome = null
        //produto.preco = 0.0

        //RESUMO
//        Tipos primitivos, ou tipos valor, sãos instanciados no stack, e classes no heap em tempo de execução.

    }
}
